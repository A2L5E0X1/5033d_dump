#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:12420765:3c90d4b930d99cc342bcf0702ecb72f4d27d27f2; then
  (applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:8076957:f35abda6c5cee9e848a66f0a241784ec4230c152 EMMC:/dev/block/platform/bootdevice/by-name/recovery 3c90d4b930d99cc342bcf0702ecb72f4d27d27f2 12420765 f35abda6c5cee9e848a66f0a241784ec4230c152:/system/recovery-from-boot.p || applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot2:8076957:f35abda6c5cee9e848a66f0a241784ec4230c152 EMMC:/dev/block/platform/bootdevice/by-name/recovery 3c90d4b930d99cc342bcf0702ecb72f4d27d27f2 12420765 f35abda6c5cee9e848a66f0a241784ec4230c152:/system/recovery-from-boot.p) && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery1:12420765:3c90d4b930d99cc342bcf0702ecb72f4d27d27f2; then
  (applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:8076957:f35abda6c5cee9e848a66f0a241784ec4230c152 EMMC:/dev/block/platform/bootdevice/by-name/recovery1 3c90d4b930d99cc342bcf0702ecb72f4d27d27f2 12420765 f35abda6c5cee9e848a66f0a241784ec4230c152:/system/recovery-from-boot.p || applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot2:8076957:f35abda6c5cee9e848a66f0a241784ec4230c152 EMMC:/dev/block/platform/bootdevice/by-name/recovery1 3c90d4b930d99cc342bcf0702ecb72f4d27d27f2 12420765 f35abda6c5cee9e848a66f0a241784ec4230c152:/system/recovery-from-boot.p) && log -t recovery "Installing recovery1 image: succeeded" || log -t recovery "Installing  recovery1 image: failed"
else
  log -t recovery "Recovery1 image already installed"
fi
